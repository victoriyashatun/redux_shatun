const express = require('express');
const router = new express.Router();
const fs = require('fs');


/* GET home page. */
router.get('/*', (req, res, next) => {
  fs.readFile(`uploads/${req.url}`, (err, file) => {
    if (err) {
      console.log(err);
    }
    res.setHeader('Content-Type', 'image/png');
    res.end(file);
  });

  return next;
});

module.exports = router;
