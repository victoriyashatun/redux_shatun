const express = require('express');
const router = new express.Router();
const usersController = require('../controllers/users.js');
const multer = require('multer');
const upload = multer({ dest: 'uploads/' });

router.get('/:id', usersController.getById);
router.get('/delete/:id', usersController.delete);
router.put('/:id', upload.single('file'), usersController.updateUser);
router.put('/', usersController.create);
router.post('/login', usersController.logIn);
router.post('/google-auth', usersController.createFromGoogle);

module.exports = router;
