const express = require('express');
const router = new express.Router();
const newsController = require('../controllers/news.js');
const multer = require('multer');
const upload = multer({ dest: 'uploads/' });

router.get('/', newsController.getPageNews);
router.put('/', upload.single('file'), newsController.create);
router.get('/find', newsController.find);

module.exports = router;
