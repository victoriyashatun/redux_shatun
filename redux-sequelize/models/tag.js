module.exports = (sequelize, DataTypes) => {
  const Tag = sequelize.define('Tag', {
    name: DataTypes.STRING,
    NewsId: DataTypes.INTEGER,
  }, {});

  Tag.associate = function(models) {
    Tag.belongsTo(models.News);
  };

  return Tag;
};
