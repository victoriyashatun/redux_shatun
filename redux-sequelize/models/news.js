module.exports = (sequelize, DataTypes) => {
  const News = sequelize.define('News', {
    authorId: DataTypes.INTEGER,
    imageUrl: DataTypes.STRING,
    text: DataTypes.TEXT,
    title: DataTypes.STRING,
  }, {});

  News.associate = models => {
    News.belongsTo(models.User, {
      foreignKey: 'authorId',
      as: 'author',
    });
    News.hasMany(models.Tag, {
      foreignKey: 'NewsId',
      as: 'tags',
    });
  };

  return News;
};
