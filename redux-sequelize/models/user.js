module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    avatarUrl: DataTypes.STRING,
    email: DataTypes.STRING,
    familyName: DataTypes.STRING,
    googleId: DataTypes.STRING,
    name: DataTypes.STRING,
    password: DataTypes.STRING,
    token: DataTypes.STRING,
  }, {});

  User.associate = models => {
    User.hasMany(models.News, {
      foreignKey: 'authorId',
      as: 'news',
    });
  };

  return User;
};
