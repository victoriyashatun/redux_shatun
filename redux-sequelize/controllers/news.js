const { News } = require('../models');
const { Tag } = require('../models');
const { User } = require('../models');
const { Sequelize: { Op } } = require('sequelize');

const ERROR = 400;
const OK = 200;
const CREATE = 201;
const NEWS_PER_PAGE = 5;

module.exports = {
  create(req, res) {
    const { body: { tags, text, title, authorId, author } } = req;
    const tagList = tags.split(',');

    let path = '';

    if (typeof req.file !== 'undefined') {

      console.log(req.file);
      path = req.file.path;
    }

    return News
      .create({
        author,
        authorId,
        imageUrl: String(path),
        text,
        title,
      })
      .then(news => Promise.all([
        Promise.all(tagList.map(tag => Tag
          .create({ NewsId: news.id, name: tag })
          .then(newTag => ({ NewsId: newTag.NewsId, id: newTag.id, name: newTag.name })))),
        news,
        author,
      ]))
      .then(async ([tagsList, news]) => {
        const author = await User.findByPk(authorId);
        return {
          author,
          tagsList,
          news,
        }
      })
      .then(({
        author,
        tagsList,
        news,
      }) => {
        return ({
          author,
          authorId,
          id: news.id,
          imageUrl: news.imageUrl,
          tags: tagsList,
          text: news.text,
          title: news.title,
        });
      })
      .then(news => {
        res.status(CREATE).send(news);
      })
      .catch(error => res.status(ERROR).send(error));
  },

  find(req, res) {
    const { query: { type, search } } = req;

    switch (type) {
      case 'text':
        return News
          .findAll({
            attributes: [
              'id',
              'imageUrl',
              'title',
              'text',
            ],
            include: [{ as: 'tags', model: Tag }, { as: 'author', model: User }],
            order: [['id', 'desc']],
            where: { [Op.or]: [{ title: { [Op.like]: `%${search}%` } }, { text: { [Op.like]: `%${search}%` } }] },
          })
          .then(news => res.status(OK).send(news))
          .catch(error => res.status(ERROR).send(error));
      case 'tags':
        return Tag
          .findAll({ where: { name: { [Op.like]: `%${search}%` } } })
          .then(tags => {
            const newsIds = tags.map(tag => tag.NewsId);

            return News.findAll({
              attributes: [
                'id',
                'imageUrl',
                'title',
                'text',
              ],
              include: [{ model: Tag, as: 'tags' }, { as: 'author', model: User }],
              order: [['id', 'desc']],
              where: { id: { [Op.in]: newsIds } },
            })
              .then(news => res.status(OK).send(news));
          })
          .catch(error => res.status(ERROR).send(error));
      case 'author':
        return News
          .findAll({
            attributes: [
              'id',
              'imageUrl',
              'title',
              'text',
            ],
            include: [
              { as: 'tags', model: Tag },
              {
                as: 'author',
                model: User,
                where: { name: { [Op.like]: `%${search}%` } },
              },
            ],
            order: [['id', 'desc']],
          })
          .then(news => res.status(OK).send(news))
          .catch(error => res.status(ERROR).send(error));
      default:
        return res.status(ERROR).send('Uncorrect type');
    }
  },

  getPageNews(req, res) {
    let { query: { page } } = req;

    const { query: { userId } } = req;

    if (typeof page === 'undefined') {
      page = 0;
    }
    if (Number(userId) !== 0) {
      return News
        .findAll({
          include: [
            {
              model: Tag,
              as: 'tags',
            },
            { as: 'author', model: User },
          ],
          limit: NEWS_PER_PAGE + 1,
          offset: NEWS_PER_PAGE * page,
          where: { authorId: [userId] },
        })
        .then(news => res.status(OK).send({ currentPage: page, currentUser: userId, news }))
        .catch(error => res.status(ERROR).send(error));
    }

    return News
      .findAll({
        attributes: [
          'authorId',
          'id',
          'imageUrl',
          'title',
          'text',
        ],
        include: [
          {
            model: Tag,
            as: 'tags',
          },
          { as: 'author', model: User },
        ],
        limit: NEWS_PER_PAGE + 1,
        offset: NEWS_PER_PAGE * page,
        order: [['id', 'desc']],
      })
      .then(news => res.status(OK).send({ currentPage: page, currentUser: userId, news }))
      .catch(error => res.status(ERROR).send(error));
  },
};
