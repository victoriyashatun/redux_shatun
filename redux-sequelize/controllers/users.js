const jwt = require('jsonwebtoken');
const { User } = require('../models');
const passwordHash = require('password-hash');
const http = require('http');
const fs = require('fs');


const ERROR = 400;
const OK = 200;
const CREATE = 201;
const CONFLICT = 409;
const SECRET = 'some solt';

module.exports = {
  updateUser(req, res) {
    const { params: { id }, body: { name, email, familyName } } = req;
    const updateField = { email, familyName, name };

    if (typeof req.file !== 'undefined') {
      updateField.avatarUrl = req.file.path;
    }

    return User
      .findByPk(id)
      .then(user => user.update(updateField)
        .then(updateUser => res.status(CREATE).send({
          avatarUrl: updateUser.avatarUrl,
          email: updateUser.email,
          id: updateUser.id,
          name: updateUser.name,
          token: updateUser.token,
        })))
      .catch(error => {
        res.status(ERROR).send(error);
      });
  },

  createFromGoogle(req, res) {
    const { body: { avatarUrl, email, familyName, googleId, name } } = req;

    return User
      .findOne({ where: { googleId } })
      .then(user => {
        if (user) {
          res.status(OK).send({
            avatarUrl: user.avatarUrl,
            email: user.email,
            id: user.id,
            name: user.name,
            token: user.token,
          });
        } else {
          User
            .findOne({ where: { email } })
            .then(userFoundByEmail => {
              if (userFoundByEmail) {
                res.status(OK).send({
                  avatarUrl: userFoundByEmail.avatarUrl,
                  email: userFoundByEmail.email,
                  id: userFoundByEmail.id,
                  name: userFoundByEmail.name,
                  token: userFoundByEmail.token,
                });
              } else {
                User
                  .create({ avatarUrl, email, familyName, googleId, name })
                  .then(createdUser => res.status(OK).send({
                    avatarUrl: createdUser.avatarUrl,
                    email: createdUser.email,
                    id: createdUser.id,
                    name: createdUser.name,
                    token: createdUser.token,
                  }));
              }
            });
        }
      })
      .catch(error => {
        res.status(ERROR).send(error);
      });
  },

  create(req, res) {
    const { body: { login: name, email, password, passwordConfirm } } = req;
    const hash = passwordHash.generate(password);

    if (password === passwordConfirm) {
      return User
        .create({ email, name, password: hash, avatarUrl: 'uploads/default.png' })
        .then(user => {
          const token = jwt.sign({
            id: user.get('id'),
            login: user.get('login'),
          }, SECRET);

          user.update({ token })
            .then(res.status(CREATE).send({
              avatarUrl: user.avatarUrl,
              email: user.email,
              id: user.id,
              name: user.name,
              token: user.token,
            }));
        })
        .catch(error => {
          if (error.errors[0].message === 'email must be unique') {
            res.status(CONFLICT).send(error);
          } else {
            res.status(ERROR).send(error);
          }
        });
    }

    return res.status(ERROR).send('Passwords didn\'t match');
  },
  delete(req, res) {
    const { params: { id } } = req;

    return User
      .destroy({
        where: { id: Number(id) },
        attributes: {
          exclude: [
            'token',
            'password',
            'googleId',
            'createdAt',
            'updatedAt',
          ],
        },
      })
      .then(() => res.status(OK))
      .catch(error => res.status(ERROR).send(error));
  },

  getById(req, res) {
    const { params: { id } } = req;

    return User
      .findOne({
        where: { id: Number(id) },
        attributes: {
          exclude: [
            'token',
            'password',
            'googleId',
            'createdAt',
            'updatedAt',
          ],
        },
      })
      .then(user => res.status(OK).send(user))
      .catch(error => res.status(ERROR).send(error));
  },

  logIn(req, res) {
    const { body: { email, password } } = req;

    return User
      .findOne({ where: { email: String(email) } })
      .then(user => {
        if (passwordHash.verify(password, user.password)) {
          res.status(OK).send({
            user: {
              id: user.id,
              avatarUrl: user.avatarUrl,
              email: user.email,
              familyName: user.familyName,
              name: user.name,
            },
            token: user.token,
          });
        } else {
          res.status(ERROR).send('User not found');
        }
      })
      .catch(error => res.status(ERROR).send(error));
  },

  logInByGoogle(req, res) {
    const {
      _json: {
        sub: googleId,
        given_name: name,
        family_name: familyName,
        picture: avatarUrl,
      },
    } = req;

    return User
      .findOne({ where: { googleId } })
      .then(user => {
        if (user) {
          res.status(OK).send(user);
        }
        User
          .create({ avatarUrl, familyName, googleId, name })
          .then(createdUser => res.status(OK).send(createdUser));
      })
      .catch(error => {
        res.status(ERROR).send(error);
      });
  },


};
