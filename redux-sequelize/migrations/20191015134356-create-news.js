module.exports = {
  down: queryInterface => queryInterface.dropTable('News'),
  up: (queryInterface, Sequelize) => queryInterface.createTable('News', {
    authorId: { type: Sequelize.INTEGER },
    createdAt: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    imageUrl: { type: Sequelize.TEXT },
    text: { type: Sequelize.TEXT },
    title: { type: Sequelize.STRING },
    updatedAt: {
      allowNull: false,
      type: Sequelize.DATE,
    },
  }),
};
