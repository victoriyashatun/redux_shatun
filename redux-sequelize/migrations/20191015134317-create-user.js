module.exports = {
  down: queryInterface => queryInterface.dropTable('Users'),
  up: (queryInterface, Sequelize) => queryInterface.createTable('Users', {
    avatarUrl: { type: Sequelize.STRING },
    createdAt: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    email: { type: Sequelize.STRING, unique: true },
    familyName: { type: Sequelize.STRING },
    googleId: { type: Sequelize.STRING },
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    name: { type: Sequelize.STRING },
    password: { type: Sequelize.STRING },
    token: { type: Sequelize.STRING },
    updatedAt: {
      allowNull: false,
      type: Sequelize.DATE,
    },
  }),
};
