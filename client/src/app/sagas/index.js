import { takeEvery, all } from 'redux-saga/effects';
import * as userConstants from '../constants/userConstants.js';
import * as newsConstants from '../constants/newsConstants.js';
import * as workers from './workers';

function *watchNews() {
  yield takeEvery(newsConstants.GET_NEWS_PROGRESS, workers.getNewsAsync);
}

function *sendNews() {
  yield takeEvery(newsConstants.SEND_NEWS_PROGRESS, workers.sendNewsAsync);
}

function *findNews() {
  yield takeEvery(newsConstants.FOUND_NEWS_PROGRESS, workers.findNewsAsync);
}

function *getUsersNews() {
  yield takeEvery(newsConstants.GET_USERS_NEWS_PROGRESS, workers.getUsersNewsAsync);
}

function *getUser() {
  yield takeEvery(userConstants.GET_USER_PROGRESS, workers.getUserAsync);
}

function *logIn() {
  yield takeEvery(userConstants.LOGIN_PROGRESS, workers.logInUserAsync);
}

function *signIn() {
  yield takeEvery(userConstants.SIGNIN_PROGRESS, workers.signInUserAsync);
}

function *updateUser() {
  yield takeEvery(userConstants.UPDATE_USER_PROGRESS, workers.updateUserAsync);
}

function *getUserForShow() {
  yield takeEvery(userConstants.GET_USER_FOR_SHOW_PROGRESS, workers.getUserForShowAsync);
}

function *loginByGoogle() {
  yield takeEvery(userConstants.LOGIN_BY_GOOGLE_PROGRESS, workers.logInByGoogleAsync);
}

export default function *rootSaga() {
  yield all([
    watchNews(),
    getUser(),
    sendNews(),
    logIn(),
    findNews(),
    signIn(),
    getUsersNews(),
    updateUser(),
    getUserForShow(),
    loginByGoogle(),
  ]);
}
