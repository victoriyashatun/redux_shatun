import { call, put } from 'redux-saga/effects';
import axios from 'axios';

import * as newsActions from '../actions/newsActions';
import * as userActions from '../actions/userActions';


export const updateUserAsync = function *updateUserAsync(action) {
  const { payload: { id } } = action;
  const config = {
    data: action.payload.data,
    headers: { 'Content-Type': 'multipart/form-data' },
    method: 'put',
    url: `/users/${id}`,
  };

  try {
    const res = yield call(axios, config);

    yield put(newsActions.getUserAfterUpdateSuccess(res.data));
  } catch (error) {
    console.log(error);
  }
};

export const getUsersNewsAsync = function *getUsersNewsAsync(action) {
  const { payload: { id } } = action;
  const config = {
    method: 'get',
    url: `/news?page=0&userId=${id}`,
  };

  try {
    const res = yield call(axios, config);

    yield put(newsActions.setUsersNews(res.data));
  } catch (error) {
    console.log(error);
  }
};

export const signInUserAsync = function *signInUserAsync(action) {
  const config = { data: action.payload, method: 'put', url: `/users/` };

  try {
    const response = yield call(axios, config);

    yield put(userActions.signInSuccess(response.data));
    localStorage.setItem('user', response.data.id);
  } catch (error) {
    userActions.signInFail('Email already taken!');
    console.log(error);
  }
};

export const findNewsAsync = function *findNewsAsync(action) {
  const { payload: { type, searchRequest } } = action;
  const config = {
    method: 'get',
    url: `/news/find?type=${type}&&search=${searchRequest}`,
  };

  try {
    const res = yield call(axios, config);

    yield put(newsActions.foundNews(res.data));
  } catch (error) {
    console.log(error);
  }
};


export const logInByGoogleAsync = function *logInByGoogleAsync(action) {
  const config = { data: action.payload, method: 'post', url: `/users/google-auth` };

  try {
    const res = yield call(axios, config);

    yield put(userActions.logInByGoogleSuccess(res.data));
    localStorage.setItem('user', res.data.id);
  } catch (error) {
    console.log(error);
  }
};


export const logInUserAsync = function *logInUserAsync(action) {
  const config = { data: action.payload, method: 'post', url: `/users/login` };

  try {
    const res = yield call(axios, config);

    yield put(userActions.logInSuccess(res.data));

    localStorage.setItem('user', res.data.user.id);
  } catch (error) {
    console.log(error);
  }
};


export const sendNewsAsync = function *sendNewsAsync(action) {
  console.log(action.payload.getAll('file'));
  const config = {
    data: action.payload,
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    method: 'put',
    url: '/news',
  };

  try {
    const res = yield call(axios, config);

    yield put(newsActions.createNews(res.data));
  } catch (error) {
    console.log(error);
  }
};


export const getNewsAsync = function *getNewsAsync(action) {
  const { payload: { page, userId } } = action;
  const config = { method: 'get', url: `/news?page=${page}&userId=${userId}` };

  try {
    const res = yield call(axios, config);

    yield put(newsActions.setNews({ news: res.data }));
  } catch (error) {
    console.log(error);
  }
};

export const getUserAsync = function *getUserAsync(action) {
  const config = { method: 'get', url: `/users/${action.payload.id}` };

  try {
    const res = yield call(axios, config);

    yield put(userActions.setUser(res.data));
  } catch (error) {
    console.log(error);
  }
};


export const getUserForShowAsync = function *getUserForShowAsync(action) {
  const config = { method: 'get', url: `/users/${action.payload.id}` };

  try {
    const res = yield call(axios, config);

    yield put(userActions.setUserForShow(res.data));
  } catch (error) {
    console.log(error);
  }
};
