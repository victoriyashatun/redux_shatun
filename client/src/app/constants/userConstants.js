export const GET_USER_SUCCESS = 'GET_USER_SUCCESS';
export const GET_USER_FAIL = 'GET_USER_FAIL';
export const GET_USER_PROGRESS = 'GET_USER_PROGRESS';

export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAIL = 'LOGIN_FAIL';
export const LOGIN_PROGRESS = 'LOGIN_PROGRESS';

export const SIGNIN_SUCCESS = 'SIGNIN_SUCCESS';
export const SIGNIN_FAIL = 'SIGNIN_FAIL';
export const SIGNIN_PROGRESS = 'SIGNIN_PROGRESS';

export const GET_USER_FOR_SHOW_SUCCESS = 'GET_USER_FOR_SHOW_SUCCESS';
export const GET_USER_FOR_SHOW_FAIL = 'GET_USER_FOR_SHOW_FAIL';
export const GET_USER_FOR_SHOW_PROGRESS = 'GET_USER_FOR_SHOW_PROGRESS';

export const SIGNOUT_SUCCESS = 'SIGNOUT_SUCCESS';

export const UPDATE_USER_SUCCESS = 'UPDATE_USER_SUCCESS';
export const UPDATE_USER_FAIL = 'UPDATE_USER_FAIL';
export const UPDATE_USER_PROGRESS = 'UPDATE_USER_PROGRESS';

export const LOGIN_BY_GOOGLE_SUCCESS = 'LOGIN_BY_GOOGLE_SUCCESS';
export const LOGIN_BY_GOOGLE_FAIL = 'LOGIN_BY_GOOGLE_FAIL';
export const LOGIN_BY_GOOGLE_PROGRESS = 'LOGIN_BY_GOOGLE_PROGRESS';
export const GET_USER_AFTER_UPDATE_SUCCESS = 'GET_USER_AFTER_UPDATE_SUCCESS';
