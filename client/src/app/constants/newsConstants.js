export const GET_NEWS_SUCCESS = 'GET_NEWS_SUCCESS';
export const GET_NEWS_FAIL = 'GET_NEWS_FAIL';
export const GET_NEWS_PROGRESS = 'GET_NEWS_PROGRESS';

export const SEND_NEWS_SUCCESS = 'SEND_NEWS_SUCCESS';
export const SEND_NEWS_FAIL = 'SEND_NEWS_FAIL';
export const SEND_NEWS_PROGRESS = 'SEND_NEWS_PROGRESS';

export const FOUND_NEWS_SUCCESS = 'FOUND_NEWS_SUCCESS';
export const FOUND_NEWS_FAIL = 'FOUND_NEWS_FAIL';
export const FOUND_NEWS_PROGRESS = 'FOUND_NEWS_PROGRESS';

export const GET_USERS_NEWS_SUCCESS = 'GET_USERS_NEWS_SUCCESS';
export const GET_USERS_NEWS_FAIL = 'GET_USERS_NEWS_FAIL';
export const GET_USERS_NEWS_PROGRESS = 'GET_USERS_NEWS_PROGRESS';
export const GET_USER_AFTER_UPDATE_SUCCESS = 'GET_USER_AFTER_UPDATE_SUCCESS';
