import * as constants from './../constants/newsConstants';

export const getNews = data => ({ payload: data, type: constants.GET_NEWS_PROGRESS });
export const setNews = data => ({ payload: data, type: constants.GET_NEWS_SUCCESS });

export const sendNews = data => ({ payload: data, type: constants.SEND_NEWS_PROGRESS });
export const createNews = data => ({ payload: data, type: constants.SEND_NEWS_SUCCESS });

export const findNews = data => ({ payload: data, type: constants.FOUND_NEWS_PROGRESS });
export const foundNews = data => ({ payload: data, type: constants.FOUND_NEWS_SUCCESS });

export const getUsersNews = id => ({ payload: { id }, type: constants.GET_USERS_NEWS_PROGRESS });
export const setUsersNews = data => ({ payload: data, type: constants.GET_USERS_NEWS_SUCCESS });

export const getUserAfterUpdateSuccess = data => ({ payload: data, type: constants.GET_USER_AFTER_UPDATE_SUCCESS });
