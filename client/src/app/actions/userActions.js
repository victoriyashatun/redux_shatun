import * as constants from './../constants/userConstants';

export const getUser = data => ({ payload: { id: data.id }, type: constants.GET_USER_PROGRESS });
export const setUser = data => ({ payload: data, type: constants.GET_USER_SUCCESS });

export const logIn = data => ({ payload: data, type: constants.LOGIN_PROGRESS });
export const logInSuccess = data => ({ payload: data, type: constants.LOGIN_SUCCESS });

export const signIn = data => ({ payload: data, type: constants.SIGNIN_PROGRESS });
export const signInSuccess = data => ({ payload: data, type: constants.SIGNIN_SUCCESS });
export const signInFail = data => ({ payload: data, type: constants.SIGNIN_FAIL });

export const logOut = () => ({ type: constants.SIGNOUT_SUCCESS });

export const updateUser = data => ({ payload: data, type: constants.UPDATE_USER_PROGRESS });
export const updateUserSuccess = data => ({ payload: data, type: constants.UPDATE_USER_SUCCESS });

export const getUserForShow = data => ({ payload: { id: data.id }, type: constants.GET_USER_FOR_SHOW_PROGRESS });
export const setUserForShow = data => ({ payload: data, type: constants.GET_USER_FOR_SHOW_SUCCESS });

export const logInByGoogle = data => ({ payload: data, type: constants.LOGIN_BY_GOOGLE_PROGRESS });
export const logInByGoogleSuccess = data => ({ payload: data, type: constants.LOGIN_BY_GOOGLE_SUCCESS });

export const getUserAfterUpdateSuccess = data => ({ payload: data, type: constants.GET_USER_AFTER_UPDATE_SUCCESS });
