import * as constants from '../constants/userConstants';

const initialState = { currentUser: {}, errors: {}, showedUser: {}, token: '' };

export const user = (state = initialState, action) => {
  switch (action.type) {
    case constants.SIGNIN_SUCCESS:
      return { ...state, currentUser: action.payload, token: action.payload.token };
    case constants.LOGIN_SUCCESS:
      return { ...state, currentUser: action.payload.user, token: action.payload.token };
    case constants.GET_USER_SUCCESS:
      return { ...state, currentUser: action.payload, token: action.payload.token };
    case constants.SIGNOUT_SUCCESS:
      return { ...state, currentUser: {}, token: '' };
    case constants.SIGNIN_FAIL:
      return { ...state, errors: action.payload };
    case constants.GET_USER_FOR_SHOW_SUCCESS:
      return { ...state, showedUser: action.payload };
    case constants.LOGIN_BY_GOOGLE_SUCCESS:
      return { ...state, currentUser: action.payload, token: action.payload.token };
    case constants.GET_USER_AFTER_UPDATE_SUCCESS:
      return { ...state, currentUser: action.payload, token: action.payload.token };
    default:
      return state;
  }
};
