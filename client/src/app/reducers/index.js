import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import { news } from './news';
import { user } from './user';

export const rootReducer = combineReducers({
  form: formReducer,
  news,
  user,
});
