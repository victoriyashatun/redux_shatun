import { combineForms } from 'react-redux-form';

const initialUserState = {
  email: '',
  password: '',
};

export const forms = combineForms({ userForm: initialUserState });
