import * as constants from '../constants/newsConstants';

const initialState = { currentPage: 0, news: [], searchRequest: '' };

export const news = (state = initialState, action) => {
  switch (action.type) {
    case constants.GET_NEWS_SUCCESS:
      return {
        ...state,
        currentPage: action.payload.news.currentPage,
        news: action.payload.news.news,
      };
    case constants.SEND_NEWS_SUCCESS:
      return {
        ...state,
        news: [action.payload, ...state.news],
      };
    case constants.FOUND_NEWS_SUCCESS:
      return {
        ...state,
        news: action.payload,
      };
    default:
      return state;
  }
};
