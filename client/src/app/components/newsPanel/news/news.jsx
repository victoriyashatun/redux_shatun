import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Post from './post/post.jsx';
import './news.css';

const POST_PER_PAGE = 5;

class News extends Component {

  getPrevPage = () => {
    const { props: { showPrevPage } } = this;

    showPrevPage();
  }

  getNextPage = () => {
    const { props: { showNextPage } } = this;

    showNextPage();
  }

  render() {
    const { props: { displayNews, currentPage } } = this;

    let pagination = <div className="pagination" />;

    if (Number(currentPage) === 0) {
      if (displayNews.length > POST_PER_PAGE) {
        pagination = (
          <div className="pagination">
            <button type="button" className="prevPage unactive">Prev</button>
            <div className="current-page">{Number(currentPage) + 1}</div>
            <button type="button" className="nextPage active" onClick={this.getNextPage}>Next</button>
          </div>
        );
      }
    } else if (displayNews.length > POST_PER_PAGE) {
      pagination = (
        <div className="pagination">
          <button type="button" className="prevPage active" onClick={this.getPrevPage}>Prev</button>
          <div className="current-page">{Number(currentPage) + 1}</div>
          <button type="button" className="nextPage active" onClick={this.getNextPage}>Next</button>
        </div>
      );
    } else {
      pagination = (
        <div className="pagination">
          <button type="button" className="prevPage active" onClick={this.getPrevPage}>Prev</button>
          <div className="current-page">{Number(currentPage) + 1}</div>
          <button type="button" className="nextPage unactive">Next</button>
        </div>
      );
    }

    return (
      <div className="news-stripe">
        <div className="news-stripe__title">News</div>
        {displayNews.slice(0, POST_PER_PAGE).map(post => <Post news={post} key={post.id} />)}
        {pagination}
      </div>
    );
  }

}

News.propTypes = {
  currentPage: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  displayNews: PropTypes.oneOfType([PropTypes.array, PropTypes.oneOf([undefined])]).isRequired,
  showNextPage: PropTypes.func.isRequired,
  showPrevPage: PropTypes.func.isRequired,
};

export default News;
