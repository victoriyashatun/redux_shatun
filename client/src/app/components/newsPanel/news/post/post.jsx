import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import tag from './tag/Tag';
import './post.css';
import Link from 'react-router/es/Link';
import config from '../../../../../config';

class Post extends PureComponent {

  render() {
    const { props: { news: { author, authorId, tags, title, text, imageUrl } } } = this;
    const tagsMockUp = tags.map(item => tag(item));

    return (
      <div className="post">
        <div>
          <Link to={`/users/${authorId}`} onlyActiveOnIndex>{author && author.name}</Link>
        </div>
        <div className="post__title">{title}</div>
        {imageUrl && <div className="post_image"><img src={`${config.url}/${imageUrl}`} alt="Post cartoon" /></div>}
        <div className="post__body">{text}</div>
        <div className="post__footer">
          <div className="post__tags">{tagsMockUp}</div>
        </div>
      </div>
    );
  }

}
Post.propTypes = {
  news: PropTypes.shape({
    authorId: PropTypes.number.isRequired,
    author: PropTypes.objectOf.isRequired,
    imageUrl: PropTypes.string,
    tags: PropTypes.array.isRequired,
    text: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
  }),
};

Post.defaultProps = { news: { tags: [], text: '', title: '' } };

export default Post;
