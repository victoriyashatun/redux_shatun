import React from 'react';

const tag = function(tags) {
  const { name, id } = tags;

  return <div className="tags" key={id}>{name}</div>;
};

export default tag;
