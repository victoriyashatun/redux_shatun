import React, { Component } from 'react';
import { connect } from 'react-redux';
import { findNews, getNews } from '../../../actions/newsActions';
import PropTypes from 'prop-types';

import './search.css';

class Search extends Component {

  constructor(props) {
    super(props);

    this.state = {
      searchRequest: '',
      type: 'author',
    };
  }

  changeSearchRequest = event => {
    this.setState({ searchRequest: event.target.value });
  }

  changeSearchType = event => {
    this.setState({ type: event.target.value });
  }

  findNewsByTitle = event => {
    const { props: { dispatchFindNews } } = this;
    const { state: { searchRequest, type } } = this;

    console.log(this.state);

    event.preventDefault();

    dispatchFindNews({ searchRequest, type });
  }

  resetSearchForm = () => {
    const { props: { dispatchGetNews } } = this;

    dispatchGetNews({ page: 0, userId: 0 });
  }

  render() {
    return (
      <div className="search">
        <form onSubmit={this.findNewsByTitle}>
          <input type="text" placeholder="text" name="searchRequest" onChange={this.changeSearchRequest} />
          <select onChange={this.changeSearchType}>
            <option value="author">Author</option>
            <option value="text">Title/Text</option>
            <option value="tags">Tags</option>
          </select>
          <button type="submit">Find</button>
          <button type="button" onClick={this.resetSearchForm}>Reset</button>
        </form>
      </div>
    );
  }

}

Search.propTypes = {
  dispatchFindNews: PropTypes.func.isRequired,
  dispatchGetNews: PropTypes.func.isRequired,
};
const mapDispatchToProps = dispatch => ({
  dispatchFindNews: data => dispatch(findNews(data)),
  dispatchGetNews: data => dispatch(getNews(data)),
});

const mapStateToProps = store => ({ news: store.news });

export default connect(mapStateToProps, mapDispatchToProps)(Search);
