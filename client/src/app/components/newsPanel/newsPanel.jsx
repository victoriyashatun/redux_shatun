import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import News from './news/news';
import Search from './search/search';
import { getNews } from '../../actions/newsActions';

class NewsPanel extends Component {

  componentDidMount() {
    const { props: { news: { news }, dispatchGetNews, user: { currentUser } } } = this;

    if (currentUser !== 0 || news.length === 0) {
      dispatchGetNews({ page: 0, userId: 0 });
    }
  }

  nextPage = () => {
    const { props: { news: { currentPage }, dispatchGetNews } } = this;

    dispatchGetNews({ page: Number(currentPage) + 1, userId: 0 });
  }

  prevPage = () => {
    const { props: { news: { currentPage }, dispatchGetNews } } = this;

    dispatchGetNews({ page: Number(currentPage) - 1, userId: 0 });
  }

  render() {
    const { props: { news: { news, currentPage } } } = this;

    return (
      <div className="newsStripe">
        <Search />
        <News
          displayNews={news}
          currentPage={currentPage}
          showNextPage={this.nextPage}
          showPrevPage={this.prevPage}
        />
      </div>
    );
  }

}
const mapDispatchToProps = dispatch => ({ dispatchGetNews: data => dispatch(getNews(data)) });

NewsPanel.propTypes = {
  dispatchGetNews: PropTypes.func.isRequired,
  news: PropTypes.shape({
    currentPage: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    news: PropTypes.arrayOf(PropTypes.any),
  }).isRequired,
  user: PropTypes.shape({
    currentUser: PropTypes.shape({
      id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
      avatarUrl: PropTypes.string,
      email: PropTypes.string,
      familyName: PropTypes.oneOfType([PropTypes.string, PropTypes.oneOf([null])]),
      name: PropTypes.string,
    }).isRequired,
  }).isRequired,
};

const mapStateToProps = store => ({ news: store.news, user: store.user });

export default connect(mapStateToProps, mapDispatchToProps)(NewsPanel);
