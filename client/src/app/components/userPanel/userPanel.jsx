import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { reset } from 'redux-form';

import { signIn, getUser, logIn, logOut, logInByGoogle, updateUser } from '../../actions/userActions';
import { sendNews } from '../../actions/newsActions';
import UserForm from './userForm/userForm';
import UserCard from './userCard/userCard';
import CreatePost from './createPostModal/createPostModal';
import EditUserForm from './editUserForm/editUserForm';

class UserPanel extends Component {

  constructor(props) {
    super(props);

    this.state = { showCreatePostModal: false, showEditUserForm: false };
  }

  componentDidMount() {
    const id = localStorage.getItem('user');
    const { props: { dispatchGetUserByToken } } = this;

    if (id) {
      dispatchGetUserByToken({ id });
    }
  }

  logOutUser = () => {
    const { props: { dispatchLogOut } } = this;

    localStorage.removeItem('user');
    dispatchLogOut();
    browserHistory.push(`/`);
  }

  onSignIn = showSignIn => {
    const { props: { form: { syncErrors, values }, dispatchSignIn, dispatchLogIn } } = this;

    if (showSignIn) {
      if (typeof syncErrors === 'undefined') {
        dispatchSignIn(values);
        browserHistory.push(`/`);
      }
    } else {
      dispatchLogIn(values);
      browserHistory.push(`/`);
    }
  }

  onSignInWithGoogle = data => {
    const { props: { dispatchLogInByGoogle } } = this;

    dispatchLogInByGoogle(data);
  }

  showCreatePost = () => this.setState({ showCreatePostModal: true })

  hideCreatePost = () => this.setState({ showCreatePostModal: false })

  showCurrentUserPage = () => {
    const { props: { user: { currentUser: { id } } } } = this;

    browserHistory.push(`/users/${id}`);
  }

  createNewPost = file => {
    const {
      props: {
        createPost: { syncErrors, values },
        user: { currentUser: { id } },
        dispatchSendNews,
        resetCreatePostForm,
      },
    } = this;

    const data = new FormData();

    data.append('file', file);
    data.append('title', values.title);
    data.append('text', values.text);
    data.append('authorId', id);
    data.append('tags', values.tags);

    if (typeof syncErrors === 'undefined') {
      dispatchSendNews(data);
      browserHistory.push(`/`);
      this.setState({ showCreatePostModal: false });
      resetCreatePostForm();
    }
  }

  showUserEditForm = () => this.setState({ showEditUserForm: true })

  hideUserEditForm = () => this.setState({ showEditUserForm: false })

  updateUser = data => {
    const { props: { user: { currentUser: { id } }, dispatchUpdateUser } } = this;
    const sendData = new FormData();

    sendData.set('email', data.email);
    sendData.set('name', data.name);
    sendData.set('familyName', data.familyName);

    if (data.file) {
      sendData.set('file', data.file);
    }
    dispatchUpdateUser({ data: sendData, id });
    browserHistory.push(`/`);
    this.setState({ showEditUserForm: false });
  }

  render() {
    const { props: { user: { currentUser } } } = this;
    const { state: { showCreatePostModal, showEditUserForm } } = this;

    if (currentUser && currentUser.id) {
      return (
        <div>
          <UserCard
            user={currentUser}
            logOut={this.logOutUser}
            showCreatePost={this.showCreatePost}
            showUserPage={this.showCurrentUserPage}
            showUserEditForm={this.showUserEditForm}
          />
          <CreatePost
            open={showCreatePostModal}
            closeCreateModal={this.hideCreatePost}
            onSubmit={this.createNewPost}
          />
          <EditUserForm
            initialValues={{
              avatarUrl: currentUser.avatarUrl,
              email: currentUser.email,
              familyName: currentUser.familyName,
              name: currentUser.name,
            }}
            open={showEditUserForm}
            closeEditUserForm={this.hideUserEditForm}
            onSubmit={this.updateUser}
          />
        </div>
      );
    }

    return <UserForm handleSubmit={this.onSignIn} googleAuth={this.onSignInWithGoogle} />;
  }

}

UserPanel.propTypes = {
  createPost: PropTypes.shape({
    syncErrors: PropTypes.shape({
      text: PropTypes.string,
      title: PropTypes.string,
    }).isRequired,
    values: PropTypes.shape({
      avatarUrl: PropTypes.string,
      email: PropTypes.string,
      familyName: PropTypes.oneOfType([PropTypes.string, PropTypes.oneOf([null])]),
      name: PropTypes.string,
      text: PropTypes.string,
      title: PropTypes.string,
      tags: PropTypes.string,
    }).isRequired,
  }),
  dispatchGetUserByToken: PropTypes.func.isRequired,
  dispatchLogIn: PropTypes.func.isRequired,
  dispatchLogInByGoogle: PropTypes.func.isRequired,
  dispatchLogOut: PropTypes.func.isRequired,
  dispatchSendNews: PropTypes.func.isRequired,
  dispatchSignIn: PropTypes.func.isRequired,
  dispatchUpdateUser: PropTypes.func.isRequired,
  form: PropTypes.shape({
    syncErrors: PropTypes.shape({
      text: PropTypes.string,
      title: PropTypes.string,
    }).isRequired,
    values: PropTypes.shape({
      avatarUrl: PropTypes.string,
      email: PropTypes.string,
      familyName: PropTypes.oneOfType([PropTypes.string, PropTypes.oneOf([null])]),
      name: PropTypes.string,
    }),
  }),
  resetCreatePostForm: PropTypes.func.isRequired,
  user: PropTypes.shape({
    currentUser: PropTypes.shape({
      id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
      avatarUrl: PropTypes.string,
      email: PropTypes.string,
      familyName: PropTypes.oneOfType([PropTypes.string, PropTypes.oneOf([null])]),
      name: PropTypes.string,
    }).isRequired,
  }).isRequired,
};

const mapDispatchToProps = dispatch => (
  {
    dispatchGetUserByToken: data => dispatch(getUser(data)),
    dispatchLogIn: data => dispatch(logIn(data)),
    dispatchLogInByGoogle: data => dispatch(logInByGoogle(data)),
    dispatchLogOut: () => dispatch(logOut()),
    dispatchSendNews: data => dispatch(sendNews(data)),
    dispatchSignIn: data => dispatch(signIn(data)),
    dispatchUpdateUser: data => dispatch(updateUser(data)),
    resetCreatePostForm: () => dispatch(reset('createPost')),
  }
);

const mapStateToProps = store => ({
  createPost: store.form.createPost,
  form: store.form.contact,
  user: store.user,
});

export default connect(mapStateToProps, mapDispatchToProps)(UserPanel);
