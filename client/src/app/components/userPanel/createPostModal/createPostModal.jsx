import React, { Component } from 'react';
import Modal from 'react-modal';
import { Field, reduxForm } from 'redux-form';
import PropTypes from 'prop-types';

import RenderField from '../userForm/RenderField/renderField';
import './createPostModal.css';

const validate = values => {
  const errors = {};
  const MAX_TITLE = 50;
  const MIN_TITLE = 5;
  const MIN_TEXT = 5;
  const MAX_TEXT = 500;

  if (!values.title) {
    errors.title = 'Required';
  } else if (values.title.length > MAX_TITLE) {
    errors.title = 'Must be 50 characters or less';
  } else if (values.title.length < MIN_TITLE) {
    errors.title = 'Must be 5 characters or more';
  }
  if (!values.text) {
    errors.text = 'Required';
  } else if (values.text.length < MIN_TEXT) {
    errors.text = 'Must be 5 characters or more';
  } else if (values.text.length > MAX_TEXT) {
    errors.text = 'Must be 500 characters or less';
  }

  return errors;
};

class CreatePost extends Component {

  constructor(props) {
    super(props);
    this.state = { file: '', imagePreviewUrl: '' };
  }

  submitForm = event => {
    const { props: { onSubmit }, state: { file } } = this;

    event.preventDefault();

    onSubmit(file);
    this.setState({ file: '', imagePreviewUrl: '' });
  }

  closeForm = () => {
    const { props: { closeCreateModal } } = this;

    closeCreateModal();
  }

  handleImageChange = event => {
    event.preventDefault();

    const reader = new FileReader();
    const file = event.target.files[0];

    reader.onloadend = () => {
      this.setState({
        file,
        imagePreviewUrl: reader.result,
      });
    };

    reader.readAsDataURL(file);
  }

  render() {
    const { props: { open }, state: { imagePreviewUrl } } = this;

    let $imagePreview = <div className="previewText">Please select an Image for Preview</div>;

    if (imagePreviewUrl) {
      $imagePreview = <img src={imagePreviewUrl} alt="Preview " />;
    }

    return (
      <Modal
        isOpen={open}
        onAfterOpen={this.afterOpenModal}
        onRequestClose={this.closeModal}
        ariaHideApp={false}
      >
        <form onSubmit={this.submitForm} className="create-post-form">
          <button type="button" onClick={this.closeForm} className="close-create-form">X</button>
          <Field name="title" type="text" component={RenderField} label="Title" className="create-form-line" />
          <Field name="text" type="textarea" component={RenderField} label="Article" className="create-form-line" />
          <Field name="tags" type="text" component={RenderField} label="Tags" className="create-form-line" />
          <input className="fileInput" type="file" onChange={this.handleImageChange} />
          <div className="imgPreview">
            {$imagePreview}
          </div>
          <div className="create-form-line">
            <button type="submit">Submit</button>
          </div>
        </form>
      </Modal>
    );
  }

}
CreatePost.propTypes = {
  closeCreateModal: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
};

export default reduxForm({
  form: 'createPost',
  validate,
})(CreatePost);
