import React from 'react';
import PropTypes from 'prop-types';

const RenderField = ({
  input,
  label,
  type,
  meta: { touched, error },
}) => (
  <div>
    <label>{label}</label>
    <div>
      { type === 'textarea'
        ? <textarea {...input} placeholder={label} type={type} />
        : <input {...input} placeholder={label} type={type} />}
      {touched && (error && <span>{error}</span>)}
    </div>
  </div>
);

RenderField.propTypes = {
  label: PropTypes.string.isRequired,
  meta: PropTypes.shape().isRequired,
  type: PropTypes.string.isRequired,
};

export default RenderField;
