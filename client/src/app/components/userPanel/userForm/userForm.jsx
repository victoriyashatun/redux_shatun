import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import PropTypes from 'prop-types';

import RenderField from './RenderField/renderField';
import './userForm.css';

const MIN_LENGTH = 8;
const MAX_LOGIN = 15;
const MIN_LOGIN = 4;

const validate = values => {
  const errors = {};

  if (!values.login) {
    errors.login = 'Required';
  } else if (values.login.length > MAX_LOGIN) {
    errors.login = 'Must be 15 characters or less';
  } else if (values.login.length < MIN_LOGIN) {
    errors.login = 'Must be 8 characters or more';
  }
  if (!values.password) {
    errors.password = 'Required';
  } else if (values.password.length < MIN_LENGTH) {
    errors.password = 'Must be 8 characters or more';
  } else if (!values.passwordConfirm) {
    errors.passwordConfirm = 'Required';
  } else if (values.password !== values.passwordConfirm) {
    errors.passwordConfirm = 'Passwords do not match';
  }

  return errors;
};

class UserForm extends Component {

  constructor(props) {
    super(props);

    this.state = { showSignIn: true };
  }

  componentDidMount() {
    window.gapi.load('auth2', () => {
      window.gapi.auth2
        .init({ clientId: '805605868427-dkrsqds1n436gbbjd8f2th9m86o7ad3o.apps.googleusercontent.com' });
    });
  }

  setLogin = () => this.setState({ showSignIn: false })

  setSignIn = () => this.setState({ showSignIn: true })

  onSignIn = () => {
    const auth2 = window.gapi.auth2.getAuthInstance();
    const { props: { googleAuth } } = this;

    auth2.signIn().then(googleUser => {
      const profile = googleUser.getBasicProfile();
      const user = {
        avatarUrl: profile.getImageUrl(),
        email: profile.getEmail(),
        familyName: profile.getFamilyName(),
        googleId: profile.getId(),
        name: profile.getGivenName(),
      };

      googleAuth(user);
    });
  }

  signOut = () => {
    const auth2 = window.gapi.auth2.getAuthInstance();

    auth2.signOut().then(() => {
      console.log('User signed out.');
    });
  }

  submitForm = event => {
    const { props: { handleSubmit }, state: { showSignIn } } = this;

    event.preventDefault();
    handleSubmit(showSignIn);
  }

  render() {
    const { state: { showSignIn } } = this;

    return (
      <form onSubmit={this.submitForm} className="login-user-form">
        <div className="checkState">
          <button type="button" className={showSignIn ? '' : 'active'} onClick={this.setLogin}>Login</button>
          <button type="button" className={showSignIn ? 'active' : ''} onClick={this.setSignIn}>Sign in</button>
        </div>
        {showSignIn && <Field name="login" type="text" component={RenderField} label="Username" />}
        <Field name="email" type="email" component={RenderField} label="Email" />
        <Field name="password" type="password" component={RenderField} label="Password" />
        {showSignIn && <Field name="passwordConfirm" type="password" component={RenderField} label="Password again" />}
        <div>
          <button type="submit">{showSignIn ? 'Sign In' : 'Log In'}</button>
        </div>
        <div>
          <button type="button" className="g-signin2" onClick={this.onSignIn}>Sign In with Google</button>
        </div>
      </form>
    );
  }

}

UserForm.propTypes = {
  googleAuth: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

export default reduxForm({
  form: 'contact',
  validate,
})(UserForm);
