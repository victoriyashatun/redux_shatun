import React, { Component } from 'react';
import PropTypes from 'prop-types';
import config from '../../../../config';

import './userCard.css';

class UserCard extends Component {

  userLogOut = () => {
    const { props: { logOut } } = this;

    logOut();
  }

  showCreatePostModal = () => {
    const { props: { showCreatePost } } = this;

    showCreatePost();
  }

  showUserPage = () => {
    const { props: { showUserPage } } = this;

    showUserPage();
  }

  showEditUserForm = () => {
    const { props: { showUserEditForm } } = this;

    showUserEditForm();
  }

  render() {
    const { props: { user } } = this;
    const link = user.avatarUrl.includes('http') ? user.avatarUrl : `${config.url}/${user.avatarUrl}`;

    return (
      <div className="user-info-card">
        <div className="greeting">{`Hello ${user.name}`}</div>
        {user.avatarUrl && <div className="avatar"><img className="icon" src={link} alt="User's avatar" /></div>}
        <button type="button" onClick={this.showCreatePostModal}>Create new post</button>
        <button type="button" onClick={this.showUserPage}>Show my page</button>
        <button type="button" onClick={this.showEditUserForm}>Edit profile</button>
        <button type="button" onClick={this.userLogOut}>Log out</button>
      </div>
    );
  }

}

UserCard.propTypes = {
  logOut: PropTypes.func.isRequired,
  showCreatePost: PropTypes.func.isRequired,
  showUserEditForm: PropTypes.func.isRequired,
  showUserPage: PropTypes.func.isRequired,
  user: PropTypes.shape({ avatarUrl: PropTypes.string, name: PropTypes.string }).isRequired,
};

export default UserCard;
