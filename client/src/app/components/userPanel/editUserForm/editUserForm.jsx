import React, { Component } from 'react';
import Modal from 'react-modal';
import { Field, reduxForm } from 'redux-form';
import PropTypes from 'prop-types';

import RenderField from '../userForm/RenderField/renderField';
import './EditUserForm.css';

class EditUserForm extends Component {

  constructor(props) {
    super(props);
    const { props: { initialValues: { avatarUrl } } } = this;

    this.state = { file: '', imagePreviewUrl: avatarUrl };
  }

  closeForm = () => {
    const { props: { closeEditUserForm } } = this;

    closeEditUserForm();
  }

  submitForm = event => {
    const { props: { onSubmit }, state: { file } } = this;

    event.preventDefault();

    onSubmit({
      email: event.target.email.value,
      familyName: event.target.familyName.value,
      file,
      name: event.target.name.value,
    });
  }

  handleImageChange = event => {
    event.preventDefault();

    const reader = new FileReader();
    const file = event.target.files[0];

    reader.onloadend = () => {
      this.setState({
        file,
        imagePreviewUrl: reader.result,
      });
    };

    reader.readAsDataURL(file);
  }

  render() {
    const { props: { open }, state: { imagePreviewUrl } } = this;

    let $imagePreview = <div className="previewText">Please select an Image for Preview</div>;

    if (imagePreviewUrl) {
      $imagePreview = <img src={imagePreviewUrl} alt="Preview " />;
    }

    return (
      <Modal
        isOpen={open}
        onAfterOpen={this.afterOpenModal}
        onRequestClose={this.closeModal}
        ariaHideApp={false}
      >
        <form onSubmit={this.submitForm} className="edit-user-form">
          <button type="button" onClick={this.closeForm} className="close-edit-user-form">X</button>
          <Field name="email" type="text" component={RenderField} label="Email" className="edit-user-line" />
          <Field name="name" type="text" component={RenderField} label="Name" className="edit-user-line" />
          <Field name="familyName" type="text" component={RenderField} label="Family Name" className="edit-user-line" />
          <input className="fileInput" type="file" name="avatarUrl" onChange={this.handleImageChange} />
          <div className="imgPreview">
            {$imagePreview}
          </div>
          <div>
            <button type="submit">Submit</button>
          </div>
        </form>
      </Modal>
    );
  }

}

EditUserForm.propTypes = {
  closeEditUserForm: PropTypes.func.isRequired,
  initialValues: PropTypes.shape({ avatarUrl: PropTypes.string }).isRequired,
  onSubmit: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
};

export default reduxForm({ form: 'createPost' })(EditUserForm);
