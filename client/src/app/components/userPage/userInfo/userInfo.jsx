import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

class UserInfo extends PureComponent {

  render() {
    const { props: { name } } = this;

    return (
      <div>{`This is the news of ${name || 'unknow user'}`}</div>
    );
  }

}

UserInfo.propTypes = { name: PropTypes.string.isRequired };

export default UserInfo;
