import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Link from 'react-router/es/Link';
import News from '../newsPanel/news/news';
import UserInfo from './userInfo/userInfo';
import { getNews } from '../../actions/newsActions';
import { getUserForShow } from '../../actions/userActions';
import config from '../../../config';

class UserPage extends Component {

  componentDidMount() {
    const {
      props: {
        dispatchGetNews,
        dispatchGetUserForShow,
        params: { id },
      },
    } = this;

    dispatchGetNews({ page: 0, userId: id });
    dispatchGetUserForShow({ id });
  }

  nextPage = () => {
    const { id } = this.props.params;
    const { props: { news: { currentPage }, dispatchGetNews } } = this;

    dispatchGetNews({ page: Number(currentPage) + 1, userId: id });
  }

  prevPage = () => {
    const { id } = this.props.params;
    const { props: { news: { currentPage }, dispatchGetNews } } = this;

    dispatchGetNews({ page: Number(currentPage) - 1, userId: id });
  }

  render() {
    const {
      props: {
        news: { news, currentPage },
        user: { showedUser: { name, id, familyName, avatarUrl } },
        params: { id: currentId },
      },
    } = this;
    const link = String(avatarUrl).includes('http') ? avatarUrl : `${config.url}/${avatarUrl}`;

    return (
      <div className="newsStripe">
        {id !== Number(currentId) && <UserInfo name={name} />}
        <div className="userDate">
          <img className="userDateImg" src={link} alt="avatar was not dowloaded" />
          <p>{`${name} ${familyName}`}</p>
        </div>
        <Link to="/">BACK</Link>
        <News
          displayNews={news}
          currentPage={currentPage}
          showNextPage={this.nextPage}
          showPrevPage={this.prevPage}
        />
      </div>
    );
  }

}

UserPage.propTypes = {
  dispatchGetNews: PropTypes.func.isRequired,
  dispatchGetUserForShow: PropTypes.func.isRequired,
  news: PropTypes.shape({
    currentPage: PropTypes.any,
    currentUser: PropTypes.any,
    news: PropTypes.any,
  }).isRequired,
  params: PropTypes.shape({ id: PropTypes.any }).isRequired,
  user: PropTypes.shape({
    currentUser: PropTypes.shape({ id: PropTypes.number }),
    showedUser: PropTypes.shape({ name: PropTypes.string }).isRequired,
  }).isRequired,
};

const mapDispatchToProps = dispatch => (
  {
    dispatchGetNews: data => dispatch(getNews(data)),
    dispatchGetUserForShow: data => dispatch(getUserForShow(data)),
  }
);

const mapStateToProps = store => ({ news: store.news, user: store.user });

export default connect(mapStateToProps, mapDispatchToProps)(UserPage);
