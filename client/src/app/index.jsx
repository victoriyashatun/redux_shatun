import React, { PureComponent } from 'react';
import { Provider } from 'react-redux';
import { Router, Route, browserHistory } from 'react-router';
import { Switch } from 'react-router-dom';

import { store } from './store/index';
import UserPanel from './components/userPanel/userPanel';
import NewsPanel from './components/newsPanel/newsPanel';
import UserPage from './components/userPage/userPage';
import './index.css';

class App extends PureComponent {

  render() {
    return (
      <Provider store={store}>
        <div className="right-stripe">
          <UserPanel />
        </div>
        <div className="main-stripe">
          <Router history={browserHistory}>
            <Switch>
              <Route path="index" component={NewsPanel} />
              <Route path="users/:id" component={UserPage} />
              <Route path="/*" component={NewsPanel} />
            </Switch>
          </Router>
        </div>
      </Provider>
    );
  }

}

export default App;
